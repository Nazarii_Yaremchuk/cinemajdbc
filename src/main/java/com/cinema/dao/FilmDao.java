package com.cinema.dao;

import com.cinema.model.Film;

import java.util.List;

public interface FilmDao {

    Film getFilm(int id);

    List<Film> getAllFilms();

    boolean createFilm(Film film);

    boolean deleteFilm(Film film);

    boolean updateFilm(Film film);

}
