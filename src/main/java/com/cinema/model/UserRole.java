package com.cinema.model;

/**
 * Enum class to define the user role
 */
public enum UserRole {
    ADMIN,
    USER;

    @Override
    public String toString() {
        return name();
    }
}
