package com.cinema.controller;

import com.cinema.model.Actor;
import com.cinema.model.Film;
import com.cinema.model.User;
import com.cinema.service.ActorService;
import com.cinema.service.FilmService;
import com.cinema.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public class ControllerServlet extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger(ControllerServlet.class);
    private static final long serialVersionUID = 1L;
    private UserService userService;
    private FilmService filmService;
    private ActorService actorService;

    public void init() {
        userService = new UserService();
        filmService = new FilmService();
        actorService = new ActorService();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String action = request.getServletPath();

        try {

            switch (action) {
                case "/new":
                    showNewForm(request, response);
                    break;
                case "/insert":
                    insertUser(request, response);
                    break;
                case "/delete":
                    deleteUser(request, response);
                    break;
                case "/edit":
                    showEditForm(request, response);
                    break;
                case "/update":
                    updateUser(request, response);
                    break;


                case "/newFilm":
                    showNewFormFilm(request, response);
                    break;
                case "/insertFilm":
                    insertFilm(request, response);
                    break;
                case "/deleteFilm":
                    deleteFilm(request, response);
                    break;
                case "/editFilm":
                    showEditFormFilm(request, response);
                    break;
                case "/updateFilm":
                    updateFilm(request, response);
                    break;


                case "/newActor":
                    showNewFormActor(request, response);
                    break;
                case "/insertActor":
                    insertActor(request, response);
                    break;
                case "/deleteActor":
                    deleteActor(request, response);
                    break;
                case "/editActor":
                    showEditFormActor(request, response);
                    break;
                case "/updateActor":
                    updateActor(request, response);
                    break;

                case "/admin":
                    loadAdminPage(request, response);
                    break;

                default:
                    initialLoad(request, response);
                    break;
            }
        } catch (ParseException e) {
            LOG.error(e.getMessage());
        }
    }


    private void initialLoad(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/index.jsp");
        dispatcher.forward(request, response);
    }

    private void loadAdminPage(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        List<User> listUser = userService.getListOfUsers();
        List<Film> listFilm = filmService.getListOfFilm();
        List<Actor> listActor = actorService.getListOfActor();
        request.setAttribute("listUser", listUser);
        request.setAttribute("listFilm", listFilm);
        request.setAttribute("listActor", listActor);
        RequestDispatcher dispatcher = request.getRequestDispatcher("Admin.jsp");
        dispatcher.forward(request, response);
    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("UserForm.jsp");
        dispatcher.forward(request, response);
    }

    private void insertUser(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        String fullName = request.getParameter("fullName");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String dateOfBirth = request.getParameter("dateOfBirth");
        String role = request.getParameter("role");

        User newUser = new User(fullName, email, password, dateOfBirth, role);
        userService.createUser(newUser);
        response.sendRedirect("admin");
    }

    private void updateUser(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        int id = Integer.parseInt(request.getParameter("id"));
        String fullName = request.getParameter("fullName");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String dateOfBirth = request.getParameter("dateOfBirth");
        String role = request.getParameter("role");
        User user = new User(id, fullName, email, password, dateOfBirth, role);
        userService.updateUser(user);
        response.sendRedirect("admin");
    }

    private void deleteUser(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        int id = Integer.parseInt(request.getParameter("id"));

        User user = new User(id);
        userService.deleteUser(user);
        response.sendRedirect("admin");

    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        User existingUser = userService.getUser(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("UserForm.jsp");
        request.setAttribute("user", existingUser);
        dispatcher.forward(request, response);
    }

    //-----------------------------------------------------film---------------------------------------------------------


    private void showNewFormFilm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("FilmForm.jsp");
        dispatcher.forward(request, response);
    }

    private void insertFilm(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ParseException {

        String filmTitle = request.getParameter("filmTitle");
        String description = request.getParameter("description");
        Float duration = Float.valueOf(request.getParameter("duration"));
        String genre = request.getParameter("genre");
        Float rating = Float.valueOf(request.getParameter("rating"));

        Film newFilm = new Film(filmTitle, description, duration, genre, rating);
        filmService.createFilm(newFilm);
        response.sendRedirect("admin");
    }

    private void updateFilm(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        int id = Integer.parseInt(request.getParameter("id"));

        String filmTitle = request.getParameter("filmTitle");
        String description = request.getParameter("description");
        Float duration = Float.valueOf(request.getParameter("duration"));
        String genre = request.getParameter("genre");
        Float rating = Float.valueOf(request.getParameter("rating"));

        Film film = new Film(id, filmTitle, description, duration, genre, rating);
        filmService.updateFilm(film);
        response.sendRedirect("admin");
    }

    private void deleteFilm(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        int id = Integer.parseInt(request.getParameter("id"));

        Film film = new Film(id);
        filmService.deleteFilm(film);
        response.sendRedirect("admin");

    }

    private void showEditFormFilm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Film existingFilm = filmService.getFilm(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("FilmForm.jsp");
        request.setAttribute("film", existingFilm);
        dispatcher.forward(request, response);
    }


//---------------------------------------------------------actor--------------------------------------------------------


    private void showNewFormActor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("ActorForm.jsp");
        dispatcher.forward(request, response);
    }

    private void insertActor(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        String fullName = request.getParameter("fullName");
        String dateOfBirth = request.getParameter("dateOfBirth");
        String nationality = request.getParameter("nationality");


        Actor newActor = new Actor(fullName, dateOfBirth, nationality);
        actorService.createActor(newActor);
        response.sendRedirect("admin");
    }

    private void updateActor(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        int id = Integer.parseInt(request.getParameter("id"));
        String fullName = request.getParameter("fullName");
        String dateOfBirth = request.getParameter("dateOfBirth");
        String nationality = request.getParameter("nationality");

        Actor actor = new Actor(id, fullName, dateOfBirth, nationality);
        actorService.updateActor(actor);
        response.sendRedirect("admin");
    }

    private void deleteActor(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Actor actor = new Actor(id);
        actorService.deleteActor(actor);
        response.sendRedirect("admin");

    }

    private void showEditFormActor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Actor existingActor = actorService.getActor(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("ActorForm.jsp");
        request.setAttribute("actor", existingActor);
        dispatcher.forward(request, response);
    }
}
