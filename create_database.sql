-- MySQL Workbench Forward Engineering

SET
@OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET
@OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET
@OLD_SQL_MODE=@@SQL_MODE, SQL_MODE=''ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION'';

-- -----------------------------------------------------
-- Schema cinema
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `cinema` DEFAULT CHARACTER SET utf8;
USE
`cinema` ;

-- -----------------------------------------------------
-- Table `cinema`.`film`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cinema`.`film`
(
    `id`
    INT
    NOT
    NULL
    AUTO_INCREMENT,
    `film_title`
    VARCHAR
(
    40
) NOT NULL,
    `description` VARCHAR
(
    100
) NULL,
    `duration` FLOAT NULL,
    `genre` VARCHAR
(
    20
) NULL,
    `rating` FLOAT NULL,
    PRIMARY KEY
(
    `id`
))
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cinema`.`timetable`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cinema`.`timetable`
(
    `id`
    INT
    NOT
    NULL
    AUTO_INCREMENT,
    `day_time`
    DATETIME
    NOT
    NULL,
    `film_id`
    INT
    NOT
    NULL,
    PRIMARY
    KEY
(
    `id`,
    `film_id`
),
    INDEX `fk_timetable_film1_idx`
(
    `film_id` ASC
) VISIBLE,
    CONSTRAINT `fk_timetable_film1`
    FOREIGN KEY
(
    `film_id`
)
    REFERENCES `cinema`.`film`
(
    `id`
)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cinema`.`guest`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cinema`.`guest`
(
    `id`
    INT
    NOT
    NULL
    AUTO_INCREMENT,
    `first_name`
    VARCHAR
(
    45
) NULL,
    `last_name` VARCHAR
(
    45
) NULL,
    `date_of_birth` DATE NULL,
    `gender` TINYINT NULL,
    PRIMARY KEY
(
    `id`
))
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cinema`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cinema`.`user`
(
    `id`
    INT
    NOT
    NULL
    AUTO_INCREMENT,
    `full_name`
    VARCHAR
(
    40
) NOT NULL,
    `email` VARCHAR
(
    30
) NOT NULL,
    `password` VARCHAR
(
    30
) NOT NULL,
    `date_of_birth` DATE NULL,
    `role` VARCHAR
(
    5
) NOT NULL,
    PRIMARY KEY
(
    `id`
))
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cinema`.`admin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cinema`.`admin`
(
    `id`
    INT
    NOT
    NULL
    AUTO_INCREMENT,
    `first_name`
    VARCHAR
(
    20
) NOT NULL,
    `last_name` VARCHAR
(
    20
) NOT NULL,
    `email` VARCHAR
(
    30
) NOT NULL,
    `password` VARCHAR
(
    30
) NOT NULL,
    `date_of_birth` DATE NOT NULL,
    `gender` TINYINT NOT NULL,
    PRIMARY KEY
(
    `id`
))
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cinema`.`ticket`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cinema`.`ticket`
(
    `id`
    INT
    NOT
    NULL
    AUTO_INCREMENT,
    `is_bought`
    TINYINT
    NOT
    NULL,
    `row_place`
    INT
    NOT
    NULL,
    `place_number`
    INT
    NOT
    NULL,
    `user_id`
    INT
    NOT
    NULL,
    `timetable_id`
    INT
    NOT
    NULL,
    `timetable_film_id`
    INT
    NOT
    NULL,
    PRIMARY
    KEY
(
    `id`,
    `user_id`,
    `timetable_id`,
    `timetable_film_id`
),
    INDEX `fk_ticket_user1_idx`
(
    `user_id` ASC
) VISIBLE,
    INDEX `fk_ticket_timetable1_idx`
(
    `timetable_id`
    ASC,
    `timetable_film_id`
    ASC
) VISIBLE,
    CONSTRAINT `fk_ticket_user1`
    FOREIGN KEY
(
    `user_id`
)
    REFERENCES `cinema`.`user`
(
    `id`
)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_ticket_timetable1`
    FOREIGN KEY
(
    `timetable_id`,
    `timetable_film_id`
)
    REFERENCES `cinema`.`timetable`
(
    `id`,
    `film_id`
)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cinema`.`seats`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cinema`.`seats`
(
    `id`
    INT
    NOT
    NULL
    AUTO_INCREMENT,
    `row_number`
    INT
    NOT
    NULL,
    `place_number`
    INT
    NOT
    NULL,
    `timetable_id`
    INT
    NOT
    NULL,
    `timetable_film_id`
    INT
    NOT
    NULL,
    PRIMARY
    KEY
(
    `id`,
    `timetable_id`,
    `timetable_film_id`
),
    INDEX `fk_seats_timetable1_idx`
(
    `timetable_id`
    ASC,
    `timetable_film_id`
    ASC
) VISIBLE,
    CONSTRAINT `fk_seats_timetable1`
    FOREIGN KEY
(
    `timetable_id`,
    `timetable_film_id`
)
    REFERENCES `cinema`.`timetable`
(
    `id`,
    `film_id`
)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cinema`.`actor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cinema`.`actor`
(
    `id`
    INT
    NOT
    NULL
    AUTO_INCREMENT,
    `full_name`
    VARCHAR
(
    40
) NOT NULL,
    `date_of_birth` DATE NULL,
    `nationality` VARCHAR
(
    20
) NULL,
    PRIMARY KEY
(
    `id`
))
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cinema`.`film_has_actor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cinema`.`film_has_actor`
(
    `film_id`
    INT
    NOT
    NULL,
    `actor_id`
    INT
    NOT
    NULL,
    PRIMARY
    KEY
(
    `film_id`,
    `actor_id`
),
    INDEX `fk_film_has_actor_actor1_idx`
(
    `actor_id` ASC
) VISIBLE,
    INDEX `fk_film_has_actor_film_idx`
(
    `film_id` ASC
) VISIBLE,
    CONSTRAINT `fk_film_has_actor_film`
    FOREIGN KEY
(
    `film_id`
)
    REFERENCES `cinema`.`film`
(
    `id`
)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_film_has_actor_actor1`
    FOREIGN KEY
(
    `actor_id`
)
    REFERENCES `cinema`.`actor`
(
    `id`
)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;


SET
SQL_MODE=@OLD_SQL_MODE;
SET
FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET
UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
