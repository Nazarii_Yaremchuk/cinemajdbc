package com.cinema.service;

import com.cinema.dao.FilmDao;
import com.cinema.dao.FilmDaoImpl;
import com.cinema.model.Film;

import java.util.List;

/**
 * Service for Film management
 */
public class FilmService {
    private FilmDao filmDao;

    /**
     * Constructor initializes dao objects
     */
    public FilmService() {
        this.filmDao = new FilmDaoImpl();
    }

    /**
     * Method in which we receive a film by id
     *
     * @param id unique number of the film
     * @return film
     */

    public Film getFilm(int id) {
        return filmDao.getFilm(id);
    }

    /**
     * method returns films
     *
     * @return list of films
     */
    public List<Film> getListOfFilm() {
        return filmDao.getAllFilms();
    }

    /**
     * Method creates film
     *
     * @param film receives the data needed to create the film
     * @return true if film was successfully created
     */
    public boolean createFilm(Film film) {
        return filmDao.createFilm(film);
    }

    /**
     * Method of deleting films
     *
     * @param film get film data
     * @return true if film was successfully deleted
     */
    public boolean deleteFilm(Film film) {
        return filmDao.deleteFilm(film);
    }

    /**
     * Method of updating data films
     *
     * @param film get film data
     * @return true if film was successfully updated
     */
    public boolean updateFilm(Film film) {
        return filmDao.updateFilm(film);
    }
}
