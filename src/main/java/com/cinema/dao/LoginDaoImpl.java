package com.cinema.dao;

import com.cinema.model.User;
import com.cinema.model.UserRole;
import com.cinema.util.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class LoginDaoImpl implements LoginDao {
    private static final Logger LOG = LogManager.getLogger(LoginDaoImpl.class);

    @Override
    public boolean saveUser(User user) {
        Connection jdbcConnection = ConnectionManager.getConnection();

        boolean set = false;
        try {
            //Insert register data to database
            String query = "insert into user(full_name,email,password,date_of_birth,role) values(?,?,?,?,?)";

            PreparedStatement pt = jdbcConnection.prepareStatement(query);
            pt.setString(1, user.getFullName());
            pt.setString(2, user.getEmail());
            pt.setString(3, user.getPassword());
            pt.setString(4, user.getDateOfBirth());
            pt.setString(5, UserRole.USER.toString());

            pt.executeUpdate();
            set = true;
        } catch (Exception e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return set;
    }

    @Override
    public User authenticateUser(String email, String password) {
        User user = null;
        Connection jdbcConnection = ConnectionManager.getConnection();

        try {
            String query = "select * from user where email=? and password=?";
            PreparedStatement ps = jdbcConnection.prepareStatement(query);
            ps.setString(1, email);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                user = new User();
                user.setId(rs.getInt("id"));
                user.setFullName(rs.getString("full_name"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
                user.setDateOfBirth(rs.getString("date_of_birth"));
                user.setRole(rs.getString("role"));
            }
        } catch (Exception e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return user;
    }
}