<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <title><fmt:message key="tab.name"/></title>
    <link rel="stylesheet" href="//apps.bdimg.com/libs/jqueryui/1.10.4/css/jquery-ui.min.css">
    <script src="//apps.bdimg.com/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//apps.bdimg.com/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <script>

        $(function () {
            $("#datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0",
            });
        });


    </script>
</head>
<body>
<center>
    <h2>
        <a href="new"><fmt:message key="add.new.user"/></a>
        <a href="list"><fmt:message key="list.all.users"/></a>
    </h2>
</center>
<div align="center">
    <c:if test="${user != null}">
    <form action="update" method="post">
        </c:if>
        <c:if test="${user == null}">
        <form action="insert" method="post">
            </c:if>
            <table border="1" cellpadding="5">
                <caption>
                    <h2>
                        <c:if test="${user != null}">
                            Edit User
                        </c:if>
                        <c:if test="${user == null}">
                            <fmt:message key="add.new.user"/>
                        </c:if>
                    </h2>
                </caption>
                <c:if test="${user != null}">
                    <input type="hidden" name="id" value="<c:out value='${user.id}' />"/>
                </c:if>
                <tr>
                    <th><fmt:message key="full.name"/>:</th>
                    <td>
                        <input type="text" name="fullName" size="40"
                               value="<c:out value='${user.fullName}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th><fmt:message key="email"/>:</th>
                    <td>
                        <input type="text" name="email" size="30"
                               value="<c:out value='${user.email}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th><fmt:message key="password"/>:</th>
                    <td>
                        <input type="text" name="password" size="30"
                               value="<c:out value='${user.password}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th><fmt:message key="date.of.birth"/>:</th>
                    <td>
                        <input type="text" size="15" id="datepicker"
                               value="<c:out value='${user.dateOfBirth}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th><fmt:message key="role"/>:</th>
                    <td>
                        <input type="text" name="role" size="5"
                               value="<c:out value='${user.role}' />"
                        />

                        <%--                            <select id="dropdown">--%>
                        <%--                                <c:forEach var="role" items="${listRoles}">--%>
                        <%--                                    <option value="<c:out value='${role}' />"--%>
                        <%--                                            <c:if test="${param.selectValue == role})"> selected </c:if>  >--%>
                        <%--                                        <c:out value="${role}" />--%>
                        <%--                                    </option>--%>
                        <%--                                </c:forEach>--%>
                        <%--                            </select>--%>
                        <%--                            --%>
                        <%--                        <select>--%>
                        <%--                            <option value="ADMIN"<c:out value='${user.role}'/>>ADMIN</option>--%>
                        <%--                            <option value="USER"<c:out value='${user.role}'/>>USER</option>--%>
                        <%--                        </select>--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="<fmt:message key="save"/>"/>
                    </td>
                </tr>
            </table>
        </form>
        <button><a href="<%=request.getContextPath()%>/logout"><fmt:message key="logout"/></a></button>
</div>
<div class="footer-copyright text-center py-3">
    <a href="?locale=en"><fmt:message key="label.button.en"/></a>
    <a href="?locale=ua"><fmt:message key="label.button.ua"/></a>
</div>
</body>
</html>
