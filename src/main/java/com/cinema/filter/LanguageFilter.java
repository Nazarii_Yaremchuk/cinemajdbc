package com.cinema.filter;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class LanguageFilter implements Filter {
    private static final Logger LOG = LogManager.getLogger(LanguageFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;

        LOG.debug("Language filter successfully started!");

        if (req.getParameter("locale") != null) {
            req.getSession().setAttribute("lang", req.getParameter("locale"));
            LOG.debug("Locale successfully changed to " + req.getParameter("locale"));
        } else if (req.getSession().getAttribute("lang") == null) {
            req.getSession().setAttribute("lang", "en");
            LOG.debug("Locale successfully set to en");
        }

        LOG.debug("Language filter successfully end!");
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}