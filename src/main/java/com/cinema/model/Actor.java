package com.cinema.model;

/**
 * Actor model
 */
public class Actor {
    private int id;
    private String fullName;
    private String dateOfBirth;
    private String nationality;


    public Actor() {

    }

    public Actor(int id) {
        this.id = id;
    }

    public Actor(int id, String fullName, String dateOfBirth, String nationality) {
        this.id = id;
        this.fullName = fullName;
        this.dateOfBirth = dateOfBirth;
        this.nationality = nationality;
    }

    public Actor(String fullName, String dateOfBirth, String nationality) {
        this.fullName = fullName;
        this.dateOfBirth = dateOfBirth;
        this.nationality = nationality;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }
}
