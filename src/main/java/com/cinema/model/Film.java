package com.cinema.model;

/**
 * Film model
 */
public class Film {
    private int id;
    private String filmTitle;
    private String description;
    private Float duration;
    private String genre;
    private Float rating;

    public Film() {
    }

    public Film(int id) {
        this.id = id;
    }

    public Film(int id, String filmTitle, String description, Float duration, String genre, Float rating) {
        this.id = id;
        this.filmTitle = filmTitle;
        this.description = description;
        this.duration = duration;
        this.genre = genre;
        this.rating = rating;
    }

    public Film(String filmTitle, String description, Float duration, String genre, Float rating) {
        this.filmTitle = filmTitle;
        this.description = description;
        this.duration = duration;
        this.genre = genre;
        this.rating = rating;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilmTitle() {
        return filmTitle;
    }

    public void setFilmTitle(String filmTitle) {
        this.filmTitle = filmTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getDuration() {
        return duration;
    }

    public void setDuration(Float duration) {
        this.duration = duration;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }
}
