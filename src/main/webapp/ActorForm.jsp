<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <title><fmt:message key="tab.name"/></title>
</head>
<body>
<center>
    <h2>
        <a href="newActor"><fmt:message key="add.new.actor"/></a>
        <a href="listActor"><fmt:message key="list.all.actors"/></a>
    </h2>
</center>
<div align="center">
    <c:if test="${actor != null}">
    <form action="updateActor" method="post">
        </c:if>
        <c:if test="${actor == null}">
        <form action="insertActor" method="post">
            </c:if>
            <table border="1" cellpadding="5">
                <caption>
                    <h2>
                        <c:if test="${actor != null}">
                            Edit Actor
                        </c:if>
                        <c:if test="${actor == null}">
                            <fmt:message key="add.new.actor"/>
                        </c:if>
                    </h2>
                </caption>
                <c:if test="${actor != null}">
                    <input type="hidden" name="id" value="<c:out value='${actor.id}' />"/>
                </c:if>
                <tr>
                    <th><fmt:message key="full.name"/>:</th>
                    <td>
                        <input type="text" name="fullName" size="40"
                               value="<c:out value='${actor.fullName}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th><fmt:message key="date.of.birth"/>:</th>
                    <td>
                        <input type="text" name="dateOfBirth" size="11"
                               value="<c:out value='${actor.dateOfBirth}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th><fmt:message key="nationality"/>:</th>
                    <td>
                        <input type="text" name="nationality" size="20"
                               value="<c:out value='${actor.nationality}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="<fmt:message key="save"/>"/>
                    </td>
                </tr>
            </table>
        </form>
        <button><a href="<%=request.getContextPath()%>/logout"><fmt:message key="logout"/></a></button>
</div>
<div class="footer-copyright text-center py-3">
    <a href="?locale=en"><fmt:message key="label.button.en"/></a>
    <a href="?locale=ua"><fmt:message key="label.button.ua"/></a>
</div>
</body>
</html>
