package com.cinema.dao;

import com.cinema.model.Actor;

import java.util.List;

public interface ActorDao {

    Actor getActor(int id);

    List<Actor> getAllActors();

    boolean createActor(Actor actor);

    boolean deleteActor(Actor actor);

    boolean updateActor(Actor actor);

}
