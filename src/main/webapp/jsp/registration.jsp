<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Register Account</title>
    <link href="https://fonts.googleapis.com/css?family=ZCOOL+XiaoWei" rel="stylesheet">
    <style>
        <%@include file="/css/style.css" %>
    </style>
    <link rel="stylesheet" href="//apps.bdimg.com/libs/jqueryui/1.10.4/css/jquery-ui.min.css">
    <script src="//apps.bdimg.com/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//apps.bdimg.com/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <script>

        $(function () {
            $("#datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0",
            });
        });


    </script>
</head>
<body>
<div class="container">

    <div class="regbox box">
        <img class="avatar" src="https://cdn.filestackcontent.com/xtnbNPESWeYrCPMOqtFQ">
        <h1>Register Account</h1>

        <form name="form" action="/registration" method="post">
            <p>Full name</p>
            <input type="text" placeholder="User name" name="fullName" required>
            <p>User email</p>
            <input type="text" placeholder="User email" name="email" required>
            <p>Date of Birth</p>
            <input type="text" placeholder="Date of Birth" name="dateOfBirth" id="datepicker">
            <p>Password</p>
            <input type="password" placeholder="Password" name="password" required>
            <input type="submit" value="Register">
            <a href="login.jsp">Already have Account?</a>
        </form>
    </div>
</div>
</body>
</html>
