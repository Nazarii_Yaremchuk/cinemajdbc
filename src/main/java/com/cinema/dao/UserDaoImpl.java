package com.cinema.dao;

import com.cinema.model.User;
import com.cinema.model.UserRole;
import com.cinema.util.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {
    private static final Logger LOG = LogManager.getLogger(UserDaoImpl.class);

    private static final String FIND_USER_BY_EMAIL = "SELECT * FROM user WHERE email=?";
    private static final String FIND_USER_BY_ID = "SELECT * FROM user WHERE id = ?";
    private static final String FIND_ALL_USERS = "SELECT * FROM user;";
    private static final String ADD_USER = "INSERT INTO user (full_name, email, password, date_of_birth, role) " +
            "VALUES (?, ?, ?, ?, ?)";
    private static final String DELETE_USER = "DELETE FROM user WHERE id = ?";
    private static final String UPDATE_USER = "UPDATE user SET full_name = ?, email = ?, password = ? , " +
            "date_of_birth = ?, role= ? WHERE id = ?";

    @Override
    public User getUser(String email) {
        User user = null;
        Connection jdbcConnection = ConnectionManager.getConnection();

        try {
            PreparedStatement ps = jdbcConnection.prepareStatement(FIND_USER_BY_EMAIL);
            ps.setString(1, email);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                user = new User();
                user.setId(rs.getInt("id"));
                user.setFullName(rs.getString("full_name"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
                user.setDateOfBirth(rs.getString("date_of_birth"));
                user.setRole(rs.getString("role"));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return user;
    }

    @Override
    public User getUser(int id) {
        User user = null;

        Connection jdbcConnection = ConnectionManager.getConnection();

        PreparedStatement statement = null;
        try {
            statement = jdbcConnection.prepareStatement(FIND_USER_BY_ID);

            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                String fullName = resultSet.getString("full_name");
                String email = resultSet.getString("email");
                String password = resultSet.getString("password");
                String dateOfBirth = resultSet.getString("date_of_birth");
                String role = resultSet.getString("role");

                user = new User(id, fullName, email, password, dateOfBirth, role);
            }

            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return user;
    }

    @Override
    public List<User> getAllUsers() {
        List<User> listOfUsers = new ArrayList<>();
        Connection jdbcConnection = ConnectionManager.getConnection();
        try {
            Statement statement = jdbcConnection.createStatement();
            ResultSet rs = statement.executeQuery(FIND_ALL_USERS);


            while (rs.next()) {
                User user = new User();

                user.setId(rs.getInt("id"));
                user.setFullName(rs.getString("full_name"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
                user.setDateOfBirth(rs.getString("date_of_birth"));
                user.setRole(rs.getString("role"));

                listOfUsers.add(user);
            }
            statement.close();
            rs.close();
        } catch (Exception e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return listOfUsers;
    }

    @Override
    public boolean createUser(User user) {
        Connection jdbcConnection = ConnectionManager.getConnection();

        PreparedStatement statement = null;
        try {
            statement = jdbcConnection.prepareStatement(ADD_USER);

            statement.setString(1, user.getFullName());
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getDateOfBirth());
            statement.setString(5, user.getRole());

            boolean rowInserted = statement.executeUpdate() > 0;
            LOG.info("Create user with id=" + user.getId() +
                    ", fullName='" + user.getFullName() + '\'' +
                    ", email='" + user.getEmail() + '\'' +
                    ", password='" + user.getPassword() + '\'' +
                    ", dateOfBirth=" + user.getDateOfBirth() +
                    ", role='" + user.getRole() + '\'');

            return rowInserted;
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return false;
    }

    @Override
    public boolean deleteUser(User user) {
        Connection jdbcConnection = ConnectionManager.getConnection();

        PreparedStatement statement = null;
        boolean rowDeleted = false;
        try {
            statement = jdbcConnection.prepareStatement(DELETE_USER);

            statement.setInt(1, user.getId());

            rowDeleted = statement.executeUpdate() > 0;
            LOG.info("Delete user with id=" + user.getId() +
                    ", fullName='" + user.getFullName() + '\'' +
                    ", email='" + user.getEmail() + '\'' +
                    ", password='" + user.getPassword() + '\'' +
                    ", dateOfBirth=" + user.getDateOfBirth() +
                    ", role='" + user.getRole() + '\'');
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return rowDeleted;
    }

    @Override
    public boolean updateUser(User user) {
        Connection jdbcConnection = ConnectionManager.getConnection();

        PreparedStatement statement = null;
        boolean rowUpdated = false;
        try {
            statement = jdbcConnection.prepareStatement(UPDATE_USER);

            statement.setString(1, user.getFullName());
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getDateOfBirth());
            statement.setString(5, UserRole.USER.toString());
            statement.setInt(6, user.getId());
            rowUpdated = statement.executeUpdate() > 0;
            LOG.info("Update user with id=" + user.getId() +
                    ", fullName='" + user.getFullName() + '\'' +
                    ", email='" + user.getEmail() + '\'' +
                    ", password='" + user.getPassword() + '\'' +
                    ", dateOfBirth=" + user.getDateOfBirth() +
                    ", role='" + user.getRole() + '\'');
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return rowUpdated;
    }
}
