package com.cinema.dao;

import com.cinema.model.User;

public interface LoginDao {

    boolean saveUser(User user);

    User authenticateUser(String email, String password);
}