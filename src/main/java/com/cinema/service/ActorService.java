package com.cinema.service;

import com.cinema.dao.ActorDao;
import com.cinema.dao.ActorDaoImpl;
import com.cinema.model.Actor;

import java.util.List;

/**
 * Service for Actor management
 */
public class ActorService {
    private ActorDao actorDao;

    /**
     * Constructor initializes dao objects
     */
    public ActorService() {
        this.actorDao = new ActorDaoImpl();
    }

    /**
     * Method in which we receive a actor by id
     *
     * @param id unique number of the actor
     * @return actor
     */
    public Actor getActor(int id) {
        return actorDao.getActor(id);
    }

    /**
     * method returns actor
     *
     * @return list of actors
     */
    public List<Actor> getListOfActor() {
        return actorDao.getAllActors();
    }

    /**
     * Method creates actor
     *
     * @param actor receives the data needed to create the actor
     * @return true if actor was successfully created
     */
    public boolean createActor(Actor actor) {
        return actorDao.createActor(actor);
    }

    /**
     * Method of deleting actor
     *
     * @param actor get actor data
     * @return true if actor was successfully deleted
     */
    public boolean deleteActor(Actor actor) {
        return actorDao.deleteActor(actor);
    }

    /**
     * Method of updating data actor
     *
     * @param actor get actor data
     * @return true if actor was successfully updated
     */
    public boolean updateActor(Actor actor) {
        return actorDao.updateActor(actor);
    }
}
