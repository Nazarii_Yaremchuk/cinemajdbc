<%@page import="com.cinema.model.User" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% User user = (User) session.getAttribute("logUser");
    if (user == null) {
        response.sendRedirect("jsp/login.jsp");
    }
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Admin Page</title>
</head>
<body>
<center><h2>Admin's Home</h2></center>
<h1>Welcome, <%= user.getFullName() %>

</h1>
<h3>Your Account ID: <%= user.getId() %>
</h3>
<h3>Your Email: <%= user.getEmail() %>
</h3>
<h3>Your Password: <%= user.getPassword() %>
</h3>

<button><a href="<%=request.getContextPath()%>/LogoutServlet">Logout</a></button>
<button><a href="<%=request.getContextPath()%>/NewUserServlet">Add New User</a></button>

<div align="center">
    <h2>
        <a href="/NewUserServlet">Add New User</a>


    </h2>

    <table border="1" cellpadding="5">

        <caption><h2>List of users</h2></caption>
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Password</th>
            <th>Date of Birth</th>
            <th>Role</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach items="${listUsers}" var="user">
            <tr>
                <td>${user.id }</td>
                <td>${user.fullName }</td>
                <td>${user.email }</td>
                <td>${user.password }</td>
                <td>${user.dateOfBirth }</td>
                <td>${user.role }</td>
                <td>
                    <a href="/edit?id=<c:out value='${book.id}' />">Edit</a>
                    <a href="/delete?id=<c:out value='${book.id}' />">Delete</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

</body>
</html>