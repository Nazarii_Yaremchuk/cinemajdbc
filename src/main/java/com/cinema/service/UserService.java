package com.cinema.service;

import com.cinema.dao.LoginDao;
import com.cinema.dao.LoginDaoImpl;
import com.cinema.dao.UserDao;
import com.cinema.dao.UserDaoImpl;
import com.cinema.model.User;

import java.util.List;

/**
 * Service for User management
 */
public class UserService {

    private LoginDao loginDao;
    private UserDao userDao;

    /**
     * Constructor initializes dao objects
     */
    public UserService() {
        this.loginDao = new LoginDaoImpl();
        this.userDao = new UserDaoImpl();
    }

    /**
     * Method for save user
     *
     * @param user receive user data
     * @return saved user
     */
    public boolean saveUser(User user) {
        return loginDao.saveUser(user);
    }

    /**
     * Method authorizes the user by email and password
     *
     * @param email    - user's email
     * @param password - user's password
     * @return authorized user
     */
    public User authenticateUser(String email, String password) {
        return loginDao.authenticateUser(email, password);
    }

    /**
     * Method in which we receive a user by email
     *
     * @param email entered by the user
     * @return user
     */
    public User getUser(String email) {
        return userDao.getUser(email);
    }

    /**
     * Method in which we receive a user by id
     *
     * @param id entered by the user
     * @return user
     */
    public User getUser(int id) {
        return userDao.getUser(id);
    }

    /**
     * method returns users
     *
     * @return list of users
     */
    public List<User> getListOfUsers() {
        return userDao.getAllUsers();
    }

    /**
     * Method creates a user
     *
     * @param user receives the data needed to create the user
     * @return true if user was successfully created
     */
    public boolean createUser(User user) {
        return userDao.createUser(user);
    }

    /**
     * Method of deleting users
     *
     * @param user get user data
     * @return true if user was successfully deleted
     */
    public boolean deleteUser(User user) {
        return userDao.deleteUser(user);
    }

    /**
     * Method of updating data users
     *
     * @param user get user data
     * @return true if user was successfully updated
     */
    public boolean updateUser(User user) {
        return userDao.updateUser(user);
    }
}
