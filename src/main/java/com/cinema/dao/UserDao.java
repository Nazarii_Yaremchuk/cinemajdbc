package com.cinema.dao;

import com.cinema.model.User;

import java.util.List;

public interface UserDao {

    User getUser(String email);

    User getUser(int id);

    List<User> getAllUsers();

    boolean createUser(User user);

    boolean deleteUser(User user);

    boolean updateUser(User user);


}
