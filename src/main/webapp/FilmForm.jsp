<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <title><fmt:message key="tab.name"/></title>
</head>
<body>
<center>
    <h2>
        <a href="newFilm"><fmt:message key="add.new.film"/></a>
        <a href="list"><fmt:message key="list.all.films"/></a>
    </h2>
</center>
<div align="center">
    <c:if test="${film != null}">
    <form action="updateFilm" method="post">
        </c:if>
        <c:if test="${film == null}">
        <form action="insertFilm" method="post">
            </c:if>
            <table border="1" cellpadding="5">
                <caption>
                    <h2>
                        <c:if test="${film != null}">
                            Edit Film
                        </c:if>
                        <c:if test="${film == null}">
                            <fmt:message key="add.new.user"/>
                        </c:if>
                    </h2>
                </caption>
                <c:if test="${film != null}">
                    <input type="hidden" name="id" value="<c:out value='${film.id}' />"/>
                </c:if>
                <tr>
                    <th><fmt:message key="film.title"/>:</th>
                    <td>
                        <input type="text" name="filmTitle" size="40"
                               value="<c:out value='${film.filmTitle}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th><fmt:message key="description"/>:</th>
                    <td>
                        <input type="text" name="description" size="100"
                               value="<c:out value='${film.description}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th><fmt:message key="duration"/>:</th>
                    <td>
                        <input type="text" name="duration" size="15"
                               value="<c:out value='${film.duration}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th><fmt:message key="genre"/>:</th>
                    <td>
                        <input type="text" name="genre" size="20"
                               value="<c:out value='${film.genre}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th><fmt:message key="rating"/>:</th>
                    <td>
                        <input type="text" name="rating" size="10"
                               value="<c:out value='${film.rating}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="<fmt:message key="save"/>"/>
                    </td>
                </tr>
            </table>
        </form>
        <button><a href="<%=request.getContextPath()%>/logout"><fmt:message key="logout"/></a></button>
</div>
<div class="footer-copyright text-center py-3">
    <a href="?locale=en"><fmt:message key="label.button.en"/></a>
    <a href="?locale=ua"><fmt:message key="label.button.ua"/></a>
</div>
</body>
</html>
