package com.cinema.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Class for creating a connection to database
 */
public class ConnectionManager {
    private static final Logger LOG = LogManager.getLogger(ConnectionManager.class);
    private static Connection jdbcConnection;

    private static Connection connect() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new SQLException(e);
        }
        return DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/cinema", "root", "dancing123");
    }

    /**
     * Method for creating connection
     *
     * @return connection
     */
    public static Connection getConnection() {
        try {
            if (jdbcConnection == null || jdbcConnection.isClosed()) {
                try {
                    jdbcConnection = connect();
                } catch (Exception e) {
                    LOG.error(e.getMessage());
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return jdbcConnection;
    }

    /**
     * Method for close connection
     */
    public static void disconnect() {
        try {
            if (jdbcConnection != null && !jdbcConnection.isClosed()) {
                jdbcConnection.close();
            }
        } catch (SQLException ex) {
            LOG.error(ex.getMessage());
        }
    }

}