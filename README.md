### What is this repository for? ###

The system is an Internet showcase of a cinema with one hall. Used MYSQL database to store information about movies,
users, actors, ratings, and more. Implemented registration, authorization. The application can be viewed in English and
Ukrainian. Two roles have been developed: administrator and user. Customers can view a list of movies and their personal
data.

The administrator can perform the following actions:
create user, change user data, delete user, add a new movie, change the movie data, delete the movie, add a new actor,
change the actor's data, delete the actor's data. An unregistered user can see a list of movies.

### Technologies used: ###

* Maven
* Java
* Mysql
* Servlets and JSPs pages
* Tomcat
* Servlet filter for authentication
* Language filter
* Css styles.

### How to set up project? ###

* Find the file in project directory create_database.sql and take the code from it to create a database. Create a Cinema
  schema in MYSQL.
* In the ConnectionManager file, change your username and password to the database.
* Build your project via Maven commands:
  mvn clean install.
* After successful execution, start Tomcat.

@author Nazarii Yaremchuk